/* ======================================================================
    SIO2 / SLAM / Mini-projet JAVA - 10/2020 - PF
    => Mini calculatrice
    => Apprentissage SWING/JAVA
====================================================================== */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.*;
import java.awt.Graphics;
import java.lang.Object;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class describes an auth.
 */
public class Auth extends JFrame
{

    private JPanel container = new JPanel();

    private JPanel champs_container = new JPanel();
    private JPanel button_container = new JPanel();

    private JButton[] button = new JButton[1];

    private JTextField zoneID ;

    private JTextField zoneMDP ;

    public String idR; 
    public String mdpR;

    /**
     * Constructs a new instance.
     */
    public Auth()
    {
        this.setSize(350, 300);
        this.setTitle("Authentification");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        initComposantA();
        this.setContentPane(container);
        this.setVisible(true);
    }

    /**
     * Gets the id.
     *
     * @return     The id.
     */
    public String getID()
    {
        return zoneID.getText();
    }

    /**
     * Gets the mdp.
     *
     * @return     The mdp.
     */
    public String getMDP()
    {
        return zoneMDP.getText();
    }

    /**
     * Sets the id.
     *
     * @param      txt   The new value
     */
    public void setID(String txt)
    {
        zoneID.setText(txt);
    }

    /**
     * Sets the mdp.
     *
     * @param      txt   The new value
     */
    public void setMDP(String txt)
    {
        zoneMDP.setText(txt);
    }

    /**
     * Initializes the composant a.
     */
    private void initComposantA()
    {
        Font police = new Font("Arial", Font.BOLD, 15);

        zoneID = new JTextField("Identifiant");
        zoneMDP = new JTextField("Mot de Passe");

        zoneID.setFont(police);
        zoneID.setHorizontalAlignment(JTextField.LEFT);
        zoneID.setPreferredSize(new Dimension(140, 30));

        zoneMDP.setFont(police);
        zoneMDP.setHorizontalAlignment(JTextField.LEFT);
        zoneMDP.setPreferredSize(new Dimension(140, 30));


        
        button[0] = new JButton("Connexion");
        button[0].addActionListener(new ConnexionListener(this));
        champs_container.add(zoneID);
        champs_container.add(zoneMDP);
        button_container.add(button[0]);
        container.add(champs_container, BorderLayout.NORTH);
        container.add(button_container, BorderLayout.SOUTH);
    }
}

/**
 * This class describes a data.
 */
class Data extends JFrame
{
    private JPanel container = new JPanel();

    private JPanel champs_container = new JPanel();
    private JPanel button_container = new JPanel();

    private JButton[] button = new JButton[1];

    private JTextField zoneCredit ;

    private JTextField zoneNumCompte ;

    public HttpData numCompt;

    public String numCompt0;



    /**
     * Constructs a new instance.
     *
     * @param      l     { parameter_description }
     * @param      p     { parameter_description }
     */
    public Data(String l, String p)
    {
        this.setSize(350, 300);
        this.setTitle("Vos données banquaire");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        numCompt = new HttpData(this,l,p);
        System.out.println (numCompt.getSTR());
        initComposantD(numCompt.getSTR());
        this.setContentPane(container);
        this.setVisible(true);
    }

    /**
     * Initializes the composant d.
     *
     * @param      numCompt0  The number compt 0
     */
    private void initComposantD(String numCompt0)
    {
        Font police = new Font("Arial", Font.BOLD, 15);

        zoneNumCompte = new JTextField("NumCompt : "+numCompt0);

        zoneNumCompte.setEditable(false);
        zoneNumCompte.setFont(police);
        zoneNumCompte.setHorizontalAlignment(JTextField.LEFT);
        zoneNumCompte.setPreferredSize(new Dimension(140, 30));
        
        button[0] = new JButton("Deconnexion");
        button[0].addActionListener(new DeconnexionListener(this));
        champs_container.add(zoneNumCompte);
        button_container.add(button[0]);
        container.add(champs_container, BorderLayout.NORTH);
        container.add(button_container, BorderLayout.SOUTH);
    }
}

/**
 * This class describes a http co.
 */
class HttpCo
{
    private Auth aut;
    private String log;
    private String passw;

    /**
     * Constructs a new instance.
     *
     * @param      a     { parameter_description }
     * @param      l     { parameter_description }
     * @param      p     { parameter_description }
     */
    public HttpCo (Auth a, String l, String p)
    {
        this.log = l;
        this.passw = p;
        this.aut = a;

        try
        {
            URL url=new URL ("http://localhost/Auth/Auth/php/Connect.php");
            HttpURLConnection connectHTTP=(HttpURLConnection) url.openConnection ();
            connectHTTP.setRequestMethod ("POST");
            connectHTTP.setRequestProperty ("Content-Type", "application/json");
            connectHTTP.setDoOutput (true);
            try (OutputStream envoiHTTP=connectHTTP.getOutputStream ())
            {   
                String compte="{\"login\":\""+log+"\",\"password\":\""+passw+"\"}";
                envoiHTTP.write (compte.getBytes ());
                envoiHTTP.flush ();
            }
            int responseCode=connectHTTP.getResponseCode ();
            System.out.println ("Reponse HTTP :" + responseCode);
            if (responseCode == 200)
            {
                System.out.println ("=> Authentification : Ok");
                aut.setVisible(false);
                aut.dispose();
                new Data(log,passw);
            }
            else
            {

                System.out.println ("=> Authentification : NOk");
            }

        }
        catch (Exception e)
        {
            e.printStackTrace ();
        }
    }
}

/**
 * This class describes a http data.
 */
class HttpData
{
    private Data data;
    private String log;
    private String passw;
    private String request;
    private String str;
    private int responseCode; 

    /**
     * Constructs a new instance.
     *
     * @param      a     { parameter_description }
     * @param      l     { parameter_description }
     * @param      p     { parameter_description }
     */
    public HttpData (Data a, String l, String p)
    {
        this.log = l;
        this.passw = p;
        this.data = a;
        try
        {
            URL url=new URL ("http://localhost/Auth/Auth/php/Connect.php");
            HttpURLConnection connectHTTP=(HttpURLConnection) url.openConnection ();
            connectHTTP.setRequestMethod ("POST");
            connectHTTP.setRequestProperty ("Content-Type", "application/json");
            connectHTTP.setDoOutput (true);
            try (OutputStream envoiHTTP=connectHTTP.getOutputStream ())
            {   
                String compte="{\"login\":\""+log+"\",\"password\":\""+passw+"\"}";
                envoiHTTP.write (compte.getBytes ());
                envoiHTTP.flush ();
            }
           
            

            responseCode=connectHTTP.getResponseCode ();
            System.out.println ("Reponse DATA :" + responseCode);

        }
        catch (Exception e)
        {
            e.printStackTrace ();
        }
    }

    /**
     * Gets the str.
     *
     * @return     The str.
     */
    public String getSTR()
    {
        System.out.println (responseCode);
        return Integer.toString(responseCode);
    }
}

/**
 * This class describes a connexion listener.
 */
class ConnexionListener extends JButton implements ActionListener
{
    private Auth a;
    private String mdpT;
    private String idT;
    private HttpCo testCo;
    /**
     * Constructs a new instance.
     *
     * @param      classR  The class r
     */
    public ConnexionListener (Auth classR)
    {
        this.a = classR;
        addActionListener(this);
    }

    /**
     * { function_description }
     *
     * @param      arg0  The argument 0
     */
    public void actionPerformed(ActionEvent arg0)
    {
        idT = a.getID();
        mdpT = a.getMDP();
        System.out.println ("Identifiant : "+idT) ;
        System.out.println ("Mot de passe : "+mdpT) ;

        testCo = new HttpCo(a, idT, mdpT);

    }
}

/**
 * This class describes a deconnexion listener.
 */
class DeconnexionListener extends JButton implements ActionListener
{
    private Data a;

    /**
     * Constructs a new instance.
     *
     * @param      classR  The class r
     */
    public DeconnexionListener (Data classR)
    {
        this.a = classR;
        addActionListener(this);
    }

    /**
     * { function_description }
     *
     * @param      arg0  The argument 0
     */
    public void actionPerformed(ActionEvent arg0)
    {
        a.setVisible(false);
        a.dispose();
        new Auth();
    }
}